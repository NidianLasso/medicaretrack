package com.example.medicaretrack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        auth = FirebaseAuth.getInstance()

        auth = FirebaseAuth.getInstance()
        if(auth.currentUser == null){
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        val correoLogin: EditText = findViewById(R.id.correoLogin)
        val passwordLogin: EditText = findViewById(R.id.passwordLogin)
        val passwordLoginLayout: TextInputLayout = findViewById(R.id.passwordLoginLayout)
        val botonLogin: Button = findViewById(R.id.botonLogin)
        val loginProgressbar: ProgressBar = findViewById(R.id.loginProgressbar)

        val redirigirRegistro : TextView = findViewById(R.id.redirigirRegistro)

        redirigirRegistro.setOnClickListener{
            val intent = Intent(this, RegistroActivity::class.java)
            startActivity(intent)
        }
        botonLogin.setOnClickListener {
            loginProgressbar.visibility = View.VISIBLE
            passwordLoginLayout.isPasswordVisibilityToggleEnabled= true

            val correo = correoLogin.text.toString()
            val password = passwordLogin.text.toString()

            if(correo.isEmpty() || password.isEmpty()){
                if(correo.isEmpty()){
                    correoLogin.error = "Introduce tu direccion de correo"
                }
                if(password.isEmpty()){
                    passwordLogin.error = "Escribre tu contraseña"
                    passwordLoginLayout.isPasswordVisibilityToggleEnabled = false
                }
                loginProgressbar.visibility = View.GONE
                Toast.makeText(this, "ingrese datos validos",Toast.LENGTH_SHORT).show()
            }else if(!correo.matches(emailPattern.toRegex())){
                loginProgressbar.visibility = View.GONE
                correoLogin.error ="Ingrese una direccion de correo valida"
                Toast.makeText(this, "Ingrese una direccion de correo valida", Toast.LENGTH_SHORT).show()
            }else if(password.length <6){
                passwordLoginLayout.isPasswordVisibilityToggleEnabled = false
                loginProgressbar.visibility = View.GONE
                passwordLogin.error ="Ingrese una contraseña de más de 6 caracteres"
                Toast.makeText(this, "Ingrese una contraseña de más de 6 caracteres", Toast.LENGTH_SHORT).show()
            }else{
                auth.signInWithEmailAndPassword(correo, password).addOnCompleteListener {
                    if(it.isSuccessful){
                        val intent = Intent(this, AgregarActivity::class.java)
                        startActivity(intent)
                    }else{
                        Toast.makeText(this, "Algo salio mal, Intente de nuevo", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    }
}