package com.example.medicaretrack

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class ItmAdapter(private val itemList: ArrayList<Dataset>, private val clickListener: (Dataset) -> Unit) : RecyclerView.Adapter<ItmAdapter.ViewHolder>() {
    class ViewHolder(itmView: View) : RecyclerView.ViewHolder(itmView) {
        val nombreMedi: EditText = itmView.findViewById(R.id.edtNombre)
        val cadaCuanto: EditText = itmView.findViewById(R.id.edtcadaCuanto)
        val dosis: EditText = itmView.findViewById(R.id.edtDosis)
        val img: ImageView = itmView.findViewById(R.id.edtImg)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.medicamento, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentitem = itemList[position]
        holder.nombreMedi.setText(currentitem.nombreMedi.toString())
        holder.cadaCuanto.setText(currentitem.cadaCuanto.toString())
        holder.dosis.setText(currentitem.dosis.toString())
        val bytes = android.util.Base64.decode(currentitem.imagen, android.util.Base64.DEFAULT)
        val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        holder.img.setImageBitmap(bitmap)
        holder.itemView.setOnClickListener {
            // Aquí se ejecuta cuando se hace clic en el elemento
            clickListener(currentitem)
        }
    }
}
