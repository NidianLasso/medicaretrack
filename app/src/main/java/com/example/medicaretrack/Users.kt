package com.example.medicaretrack

data class Users(
    var nombre: String? = null,
    var correo: String? = null,
    var usuario: String? = null,
    var uid: String? = null
)
