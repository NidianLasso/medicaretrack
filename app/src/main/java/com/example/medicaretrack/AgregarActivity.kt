package com.example.medicaretrack

import android.app.Instrumentation.ActivityResult
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.example.medicaretrack.databinding.ActivityAgregarBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.ByteArrayOutputStream

class AgregarActivity : AppCompatActivity() {

    var sImage: String? = ""
    private lateinit var binding: ActivityAgregarBinding
    private lateinit var db: DatabaseReference
    private lateinit var imageLauncher: ActivityResultLauncher<Intent>
    private lateinit var storage: FirebaseStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAgregarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val salirBtn: Button = findViewById(R.id.salirBtn)
        val añadirBtn: Button = findViewById(R.id.añadirBtn)

        salirBtn.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        añadirBtn.setOnClickListener {
            insert_data()
        }

        imageLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                val uri = result.data!!.data
                try {
                    val inputStream = contentResolver.openInputStream(uri!!)
                    val myBitmap = BitmapFactory.decodeStream(inputStream)
                    val stream = ByteArrayOutputStream()
                    myBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    val bytes = stream.toByteArray()
                    sImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                    binding.subirImg.setImageBitmap(myBitmap)
                    inputStream!!.close()
                    Toast.makeText(this, "Imagen seleccionada", Toast.LENGTH_SHORT).show()

                } catch (ex: Exception) {
                    Toast.makeText(this, ex.message.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }

        storage = FirebaseStorage.getInstance()
    }

    private fun insert_data() {
        val nombreMedi = binding.editNombre.text.toString()
        val cadaCuanto = binding.edtcadaCuanto.text.toString()
        val dosis = binding.edtDosis.text.toString()

        if (TextUtils.isEmpty(nombreMedi) || TextUtils.isEmpty(cadaCuanto) || TextUtils.isEmpty(dosis)) {
            Toast.makeText(this, "Por favor, complete todos los campos", Toast.LENGTH_SHORT).show()
            return
        }

        if (TextUtils.isEmpty(sImage)) {
            Toast.makeText(this, "Por favor, seleccione una imagen", Toast.LENGTH_SHORT).show()
            return
        }

        db = FirebaseDatabase.getInstance().getReference("medicamentos")
        val item = Dataset(nombreMedi, cadaCuanto, dosis, sImage)
        val databaseReference = FirebaseDatabase.getInstance().reference
        val id = databaseReference.push().key

        val storageRef: StorageReference = storage.reference.child("imagenes")
        val imageFileName = System.currentTimeMillis().toString() + ".png"
        val imageRef: StorageReference = storageRef.child(imageFileName)
        val decodedImageBytes: ByteArray = Base64.decode(sImage, Base64.DEFAULT)

        val uploadTask = imageRef.putBytes(decodedImageBytes)
        uploadTask.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                imageRef.downloadUrl.addOnSuccessListener { uri ->
                    val imageUrl = uri.toString()
                    item.imageUrl = imageUrl

                    db.child(id.toString()).setValue(item).addOnSuccessListener {
                        binding.editNombre.text.clear()
                        binding.edtcadaCuanto.text.clear()
                        binding.edtDosis.text.clear()
                        sImage = ""
                        Toast.makeText(this, "Medicamento insertado", Toast.LENGTH_SHORT).show()
                    }.addOnFailureListener {
                        Toast.makeText(this, "Fallo en insertar el medicamento", Toast.LENGTH_SHORT).show()
                    }
                }.addOnFailureListener {
                    Toast.makeText(this, "Error al obtener la URL de descarga de la imagen", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "Error al subir la imagen", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun insert_image(view: View) {
        val myfileintent = Intent(Intent.ACTION_GET_CONTENT)
        myfileintent.type = "image/*"
        imageLauncher.launch(myfileintent)
    }

    fun mostar_listado(view: View) {
        var i: Intent
        i = Intent(this, ListadoActivity::class.java)
        startActivity(i)
    }
}
