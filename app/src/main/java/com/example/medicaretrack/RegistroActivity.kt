package com.example.medicaretrack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class RegistroActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private val emailPattern = "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()

        val nombreRegistro : EditText = findViewById(R.id.nombreRegistro)
        val correoRegistro : EditText = findViewById(R.id.correoRegistro)
        val usuarioRegistro : EditText = findViewById(R.id.usuarioRegistro)
        val passwordRegistro : EditText = findViewById(R.id.passwordRegistro)
        val confirmarPsw : EditText = findViewById(R.id.confirmarPsw)
        val passworRegistroLayout: TextInputLayout = findViewById(R.id.passwordRegistroLayout)
        val confirmarPswLayout : TextInputLayout = findViewById(R.id.confirmarPswLayout)
        val botonRegistrar : Button = findViewById(R.id.botonRegistrar)
        val registroProgressBar : ProgressBar = findViewById(R.id.registroProgressBar)

        val redirigirLogin : TextView = findViewById(R.id.redirigirLogin)

        redirigirLogin.setOnClickListener{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        botonRegistrar.setOnClickListener {
            val nombre = nombreRegistro.text.toString()
            val correo = correoRegistro.text.toString()
            val usuario = usuarioRegistro.text.toString()
            val password = passwordRegistro.text.toString()
            val cPsw = confirmarPsw.text.toString()

            registroProgressBar.visibility = View.VISIBLE
            passworRegistroLayout.isPasswordVisibilityToggleEnabled = true
            confirmarPswLayout.isPasswordVisibilityToggleEnabled = true

            if (nombre.isEmpty() || correo.isEmpty() || usuario.isEmpty() || password.isEmpty() || cPsw.isEmpty()) {
                if (nombre.isEmpty()) {
                    nombreRegistro.error = "Ingrese su nombre"
                }
                if (correo.isEmpty()) {
                    correoRegistro.error = "Ingrese su correo"
                }
                if (usuario.isEmpty()) {
                    usuarioRegistro.error = "Ingrese su usuario"
                }
                if (password.isEmpty()) {
                    passworRegistroLayout.isPasswordVisibilityToggleEnabled = false
                    passwordRegistro.error = "Ingrese su contraseña"

                }
                if (cPsw.isEmpty()) {
                    confirmarPswLayout.isPasswordVisibilityToggleEnabled = false
                    confirmarPsw.error = "Confirme su contraseña"

                }
                Toast.makeText(this, "Ingrese datos validos", Toast.LENGTH_SHORT).show()
                registroProgressBar.visibility = View.GONE
            }else if(!correo.matches(emailPattern.toRegex())){
                registroProgressBar.visibility = View.GONE
                correoRegistro.error ="Ingrese una direccion de correo valida"
                Toast.makeText(this, "Ingrese una direccion de correo valida", Toast.LENGTH_SHORT).show()
            }else if(password.length <6){
                passworRegistroLayout.isPasswordVisibilityToggleEnabled = false
                registroProgressBar.visibility = View.GONE
                passwordRegistro.error ="Ingrese una contraseña de más de 6 caracteres"
                Toast.makeText(this, "Ingrese una contraseña de más de 6 caracteres", Toast.LENGTH_SHORT).show()
            }else if(password != cPsw){
                confirmarPswLayout.isPasswordVisibilityToggleEnabled = false
                registroProgressBar.visibility = View.GONE
                confirmarPsw.error ="Contraseñas no coinciden"
                Toast.makeText(this, "Contraseñas no coinciden", Toast.LENGTH_SHORT).show()
            }else{
                auth.createUserWithEmailAndPassword(correo, password).addOnCompleteListener {
                    if(it.isSuccessful){
                        val databaseRef = database.reference.child("users").child(auth.currentUser!!.uid)
                        val users: Users = Users(nombre, correo, usuario, auth.currentUser!!.uid)

                        databaseRef.setValue(users).addOnCompleteListener {
                            if(it.isSuccessful){
                                val intent = Intent(this, LoginActivity::class.java)
                                startActivity(intent)
                            }else{
                                Toast.makeText(this,"Algo salio mal, Intente de nuevo", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }else{
                        Toast.makeText(this,"Algo salio mal, Intente de nuevo", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}