package com.example.medicaretrack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class ListadoActivity : AppCompatActivity() {

    private lateinit var db: DatabaseReference
    private lateinit var itemRecyclerView: RecyclerView
    private lateinit var itemArrayList: ArrayList<Dataset>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listado)
        itemRecyclerView = findViewById(R.id.listado)
        itemRecyclerView.layoutManager = LinearLayoutManager(this)
        itemRecyclerView.setHasFixedSize(true)
        itemArrayList = arrayListOf<Dataset>()
        getItemData()
        val regresarBtn: Button = findViewById(R.id.regresarBtn)

        regresarBtn.setOnClickListener {
            val intent = Intent(this, AgregarActivity::class.java)
            startActivity(intent)
        }
    }

    private fun getItemData() {
        db = FirebaseDatabase.getInstance().getReference("medicamentos")
        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    itemArrayList.clear()
                    for (itmsnapshot in snapshot.children) {
                        val item = itmsnapshot.getValue(Dataset::class.java)
                        item?.id = itmsnapshot.key // Asignar el ID del nodo a la propiedad "id" del objeto Dataset
                        itemArrayList.add(item!!)
                    }
                    itemRecyclerView.adapter = ItmAdapter(itemArrayList) { medicamento ->
                        eliminarMedicamento(medicamento.id)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                // Manejar el error aquí
            }
        })
    }

    private fun eliminarMedicamento(id: String?) {
        if (id != null) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Eliminar Medicamento")
            builder.setMessage("¿Estás seguro de que deseas eliminar este medicamento?")
            builder.setPositiveButton("Sí") { _, _ ->
                // Eliminar el medicamento
                db.child(id).removeValue()
                    .addOnSuccessListener {
                        Toast.makeText(this, "Medicamento eliminado con exito", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "Error al eliminar el medicamento", Toast.LENGTH_SHORT).show()
                    }
            }
            builder.setNegativeButton("No") { _, _ ->
                // No hacer nada, el usuario ha cancelado la eliminación del medicamento
            }
            val dialog = builder.create()
            dialog.show()
        }
    }


}
