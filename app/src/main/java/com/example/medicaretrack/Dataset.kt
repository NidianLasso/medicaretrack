package com.example.medicaretrack

data class Dataset(
    val nombreMedi: String? = null,
    val cadaCuanto: String? = null,
    val dosis: String? = null,
    val imagen: String? = null,
    var imageUrl: String? = null,
    var id: String? = null
)
